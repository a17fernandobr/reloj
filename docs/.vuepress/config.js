module.exports = {
 title: "Creación de componentes swing con Netbeans",
 description: "Descricion",
 dest: "public",
 base: "/reloj/",
 themeConfig: {
     sidebar: [
      ['/paxina1', 'Reloj'],
      ['/paxina2', 'Alarma']
     ],
     nav: [
      { text: 'Home', link: '/' },
      { text: 'Reloj', link: '/paxina1' },
      { text: 'Alarma', link: '/paxina2' },
      { text: 'External', link: 'https://google.com' }
     ],
     lastUpdated: 'Last Updated', // string | boolean
 }
}
