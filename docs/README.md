---
home: true
heroImage: /casio.jpg
heroText: Reloj Digital
tagline: con Alarma
actionText: Get Started →
actionLink: /paxina1/
features:
- title: Componente Simple
  details: Creación de un reloj digital a partir de un JLabel.
- title: Componente complejo
  details: Vamos a añadir una alarma a nuestro reloj.
footer: MIT Licensed | Copyright © 2018-present Evan You
---