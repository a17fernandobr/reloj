RELOJ DIGITAL :watch:
===
Vamos a crear un componente swing, un reloj al que le podemos cambiar el formato de hora entre 24h y 12h a partir de un JLabel.

Hay muchas maneras de crear este componente, en nuestro caso vamos a usar un booleano para cambiar el formato de 12 a 24 horas y la clase **Timer** de swing con su import correspondiente, *javax.swing.Timer*. 
Para tomar la hora usaremos **Date**, también se podría hacer con Calendar...


Creación de componente
-------
En el proyecto de NetBeans creamos una clase, en nuestro caso la hemos llamado _RelojDigital_, esta clase debe extender del componente *JLabel* e implementar la interfaz *Serializable*.

Añadimos un atributo booleano, doceHoras, que nos dira en que formato queremos la hora. Creamos el setter y getter correspondiente a este atributo.

Necesitamos que nuestro evento se realice cada segundo, para eso vamos a usar la clase Timer que se construye con el intervalo de tiempo en milisegundos que se va a repetir nuestro código y un listener:
``` java{13}
Timer timer = new Timer(1000, new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    // Tu Codigo
                }
            });
```
Ahora debemos añadir el código que queremos que se ejecute en cada repetición, para coger la hora vamos a usar la clase Date y le daremos formato con DateFormat y SimpleDateFormat. Para verlo en 12 o 24 horas tan solo debemos cambiar el formato en el constructor, 'HH:mm:ss' para 24 horas y 'hh:mm:ss a' para 12 horas la a añadida es para que ponga am o pm. Nos quedaría el siguiente código para añadir dentro de Timer:
``` java{13}
DateFormat hora;
            if (isDoceHoras()) {
                hora = new SimpleDateFormat("hh:mm:ss a");
            } else {
                hora = new SimpleDateFormat("HH:mm:ss");
            }
            Date d = new Date();
            setText(hora.format(d));
```
Una vez definido el Timer es necesario inicializarlo, timer.start(), lo haremos dentro del constructor de nuestra clase. Nos quedaría así:

::: details Código
``` java{13}
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.Timer;


public class RelojDigital extends JLabel implements Serializable {

    private boolean doceHoras;

    public boolean isDoceHoras() {
        return doceHoras;

    }

    public void setDoceHoras(boolean doceHoras) {
        this.doceHoras = doceHoras;
    }

    public RelojDigital() {

        t.start();

    }
    Timer t = new Timer(1000, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent ae) {
            DateFormat hora;
            if (isDoceHoras()) {
                hora = new SimpleDateFormat("hh:mm:ss a");
            } else {
                hora = new SimpleDateFormat("HH:mm:ss");
            }
            Date d = new Date();
            setText(hora.format(d));

        }

    });
}
```
:::


Con nuestro componente creado solo nos queda añadirlo a la paleta de NetBeans, lo primero hacemos *Clean and build* en nuestro proyecto, clicamos con el botón derecho sobre nuestra clase y en *Tools* escogemos la opción *Add to palette*. Nos abre una ventana con las categorías de la paleta, seleccionamos la categoría elegida y damos ok.

Ya solo nos queda probar nuestro componente.:sleepy: